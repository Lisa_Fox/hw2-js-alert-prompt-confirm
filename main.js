//ТЕОРЕТИЧНІ ПИТАННЯ
//1. У JavaScript є існують наступні типи даних:
// Number - для будь-яких чисел (цілих або з рухомою точкою) та цілі числа обмежені до ±(253-1).
// Bigint - для цілих чисел довільної довжини.
// String -  рядок, символьний тип даних, тобто послідовність символів.
// Boolean - булеві змінні true/false.
// Object - для більш складних структур даних.
// Symbol - примітивний тип даних (новий тип даних який появився в специфікації ES6)
// Null - автономний тип, який має єдине значення null (для невідомих значень).
// Undefined - автономний тип, який має єдине значення undefined (для неприсвоєних значень).

// 2. Різниця між == та === полягає у тому, що оператор === порівнює на рівність, а == на ідентичність.

// 3. Оператор – це внутрішня функція JavaScript, тобто інструкція даної мови програмування,
//якою задається певний крок процесу обробки інформації. Оператори буають 3 типів:
// Унарні - вимагає один оператор і встановлюється перед або після нього;
// Бінарні - вимагають двох операторів і встановлюється між ними;
//Тернарні - Вимагають 3-х операторів; (Тернарний оператор є аналогом умовного оператора If...Else.)



let userName;
let age;
while (!userName || !age || typeof age === "string") {
  userName = prompt("Enter your name", "Valera");
  age = +prompt("Enter your age", "86");
}
if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  let userResult = confirm("Are you sure you want to continue?");
  if (userResult === true) {
    alert("Welcome, " + userName);
  } else if (userResult === false) {
    alert("You are not allowed to visit this website");
  }
} else if (age > 22) {
  alert(`Welcome, ${userName}`);
}
